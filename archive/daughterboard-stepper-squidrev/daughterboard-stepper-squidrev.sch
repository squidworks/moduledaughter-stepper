<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="power">
<packages>
<package name="QFN32">
<description>&lt;b&gt;QFN 32&lt;/b&gt; 5 x 5 mm&lt;p&gt;
Source: http://datasheets.maxim-ic.com/en/ds/MAX7042.pdf</description>
<wire x1="-2.45" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="2.05" x2="-2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="2.45" x2="-2.05" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.05" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="2.05" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.05" x2="2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.45" x2="2.05" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.05" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="-2.05" width="0.1016" layer="21"/>
<circle x="-2.175" y="2.175" radius="0.15" width="0" layer="21"/>
<smd name="EXP" x="0" y="0" dx="3.2" dy="3.2" layer="1" stop="no" cream="no"/>
<smd name="1" x="-2.325" y="1.75" dx="0.55" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="2" x="-2.3" y="1.25" dx="0.6" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="3" x="-2.3" y="0.75" dx="0.6" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="4" x="-2.3" y="0.25" dx="0.6" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="5" x="-2.3" y="-0.25" dx="0.6" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="6" x="-2.3" y="-0.75" dx="0.6" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="7" x="-2.3" y="-1.25" dx="0.6" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="8" x="-2.325" y="-1.75" dx="0.55" dy="0.25" layer="1" stop="no" cream="no"/>
<smd name="9" x="-1.75" y="-2.325" dx="0.55" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="10" x="-1.25" y="-2.3" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="11" x="-0.75" y="-2.3" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="12" x="-0.25" y="-2.3" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="13" x="0.25" y="-2.3" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="14" x="0.75" y="-2.3" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="15" x="1.25" y="-2.3" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="16" x="1.75" y="-2.325" dx="0.55" dy="0.25" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="17" x="2.325" y="-1.75" dx="0.55" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="18" x="2.3" y="-1.25" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="19" x="2.3" y="-0.75" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="20" x="2.3" y="-0.25" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="21" x="2.3" y="0.25" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="22" x="2.3" y="0.75" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="23" x="2.3" y="1.25" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="24" x="2.325" y="1.75" dx="0.55" dy="0.25" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="25" x="1.75" y="2.325" dx="0.55" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="26" x="1.25" y="2.3" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="27" x="0.75" y="2.3" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="28" x="0.25" y="2.3" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="29" x="-0.25" y="2.3" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="30" x="-0.75" y="2.3" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="31" x="-1.25" y="2.3" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="32" x="-1.75" y="2.325" dx="0.55" dy="0.25" layer="1" rot="R270" stop="no" cream="no"/>
<text x="-4.05" y="-4.35" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.8" y="3.25" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.3" y1="1.1" x2="0.3" y2="1.4" layer="31"/>
<rectangle x1="-0.3" y1="0.6" x2="0.3" y2="0.9" layer="31"/>
<rectangle x1="-0.3" y1="0.1" x2="0.3" y2="0.4" layer="31"/>
<rectangle x1="-0.3" y1="-0.4" x2="0.3" y2="-0.1" layer="31"/>
<rectangle x1="-0.3" y1="-0.9" x2="0.3" y2="-0.6" layer="31"/>
<rectangle x1="-0.3" y1="-1.4" x2="0.3" y2="-1.1" layer="31"/>
<rectangle x1="-1.3" y1="1.1" x2="-0.7" y2="1.4" layer="31"/>
<rectangle x1="-1.3" y1="0.6" x2="-0.7" y2="0.9" layer="31"/>
<rectangle x1="-1.3" y1="0.1" x2="-0.7" y2="0.4" layer="31"/>
<rectangle x1="-1.3" y1="-0.4" x2="-0.7" y2="-0.1" layer="31"/>
<rectangle x1="-1.3" y1="-0.9" x2="-0.7" y2="-0.6" layer="31"/>
<rectangle x1="-1.3" y1="-1.4" x2="-0.7" y2="-1.1" layer="31"/>
<rectangle x1="0.7" y1="1.1" x2="1.3" y2="1.4" layer="31"/>
<rectangle x1="0.7" y1="0.6" x2="1.3" y2="0.9" layer="31"/>
<rectangle x1="0.7" y1="0.1" x2="1.3" y2="0.4" layer="31"/>
<rectangle x1="0.7" y1="-0.4" x2="1.3" y2="-0.1" layer="31"/>
<rectangle x1="0.7" y1="-0.9" x2="1.3" y2="-0.6" layer="31"/>
<rectangle x1="0.7" y1="-1.4" x2="1.3" y2="-1.1" layer="31"/>
<rectangle x1="-2.5" y1="0.25" x2="-0.25" y2="2.5" layer="51"/>
<polygon width="0.5" layer="29">
<vertex x="-1.325" y="1.175"/>
<vertex x="-1.175" y="1.325"/>
<vertex x="1.325" y="1.325"/>
<vertex x="1.325" y="-1.325"/>
<vertex x="-1.325" y="-1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="1.85"/>
<vertex x="-2.1" y="1.85"/>
<vertex x="-2.05" y="1.8"/>
<vertex x="-2.05" y="1.65"/>
<vertex x="-2.55" y="1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="1.825"/>
<vertex x="-2.125" y="1.825"/>
<vertex x="-2.075" y="1.775"/>
<vertex x="-2.075" y="1.675"/>
<vertex x="-2.525" y="1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="1.35"/>
<vertex x="-2.05" y="1.35"/>
<vertex x="-2.05" y="1.15"/>
<vertex x="-2.55" y="1.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="1.325"/>
<vertex x="-2.075" y="1.325"/>
<vertex x="-2.075" y="1.175"/>
<vertex x="-2.525" y="1.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="0.85"/>
<vertex x="-2.05" y="0.85"/>
<vertex x="-2.05" y="0.65"/>
<vertex x="-2.55" y="0.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="0.825"/>
<vertex x="-2.075" y="0.825"/>
<vertex x="-2.075" y="0.675"/>
<vertex x="-2.525" y="0.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="0.35"/>
<vertex x="-2.05" y="0.35"/>
<vertex x="-2.05" y="0.15"/>
<vertex x="-2.55" y="0.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="0.325"/>
<vertex x="-2.075" y="0.325"/>
<vertex x="-2.075" y="0.175"/>
<vertex x="-2.525" y="0.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-0.15"/>
<vertex x="-2.05" y="-0.15"/>
<vertex x="-2.05" y="-0.35"/>
<vertex x="-2.55" y="-0.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-0.175"/>
<vertex x="-2.075" y="-0.175"/>
<vertex x="-2.075" y="-0.325"/>
<vertex x="-2.525" y="-0.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-0.65"/>
<vertex x="-2.05" y="-0.65"/>
<vertex x="-2.05" y="-0.85"/>
<vertex x="-2.55" y="-0.85"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-0.675"/>
<vertex x="-2.075" y="-0.675"/>
<vertex x="-2.075" y="-0.825"/>
<vertex x="-2.525" y="-0.825"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-1.15"/>
<vertex x="-2.05" y="-1.15"/>
<vertex x="-2.05" y="-1.35"/>
<vertex x="-2.55" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-1.175"/>
<vertex x="-2.075" y="-1.175"/>
<vertex x="-2.075" y="-1.325"/>
<vertex x="-2.525" y="-1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-1.85"/>
<vertex x="-2.1" y="-1.85"/>
<vertex x="-2.05" y="-1.8"/>
<vertex x="-2.05" y="-1.65"/>
<vertex x="-2.55" y="-1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-1.825"/>
<vertex x="-2.125" y="-1.825"/>
<vertex x="-2.075" y="-1.775"/>
<vertex x="-2.075" y="-1.675"/>
<vertex x="-2.525" y="-1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.85" y="-2.55"/>
<vertex x="-1.85" y="-2.1"/>
<vertex x="-1.8" y="-2.05"/>
<vertex x="-1.65" y="-2.05"/>
<vertex x="-1.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.825" y="-2.525"/>
<vertex x="-1.825" y="-2.125"/>
<vertex x="-1.775" y="-2.075"/>
<vertex x="-1.675" y="-2.075"/>
<vertex x="-1.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.35" y="-2.55"/>
<vertex x="-1.35" y="-2.05"/>
<vertex x="-1.15" y="-2.05"/>
<vertex x="-1.15" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.325" y="-2.525"/>
<vertex x="-1.325" y="-2.075"/>
<vertex x="-1.175" y="-2.075"/>
<vertex x="-1.175" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.85" y="-2.55"/>
<vertex x="-0.85" y="-2.05"/>
<vertex x="-0.65" y="-2.05"/>
<vertex x="-0.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.825" y="-2.525"/>
<vertex x="-0.825" y="-2.075"/>
<vertex x="-0.675" y="-2.075"/>
<vertex x="-0.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.35" y="-2.55"/>
<vertex x="-0.35" y="-2.05"/>
<vertex x="-0.15" y="-2.05"/>
<vertex x="-0.15" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.325" y="-2.525"/>
<vertex x="-0.325" y="-2.075"/>
<vertex x="-0.175" y="-2.075"/>
<vertex x="-0.175" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.15" y="-2.55"/>
<vertex x="0.15" y="-2.05"/>
<vertex x="0.35" y="-2.05"/>
<vertex x="0.35" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.175" y="-2.525"/>
<vertex x="0.175" y="-2.075"/>
<vertex x="0.325" y="-2.075"/>
<vertex x="0.325" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.65" y="-2.55"/>
<vertex x="0.65" y="-2.05"/>
<vertex x="0.85" y="-2.05"/>
<vertex x="0.85" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.675" y="-2.525"/>
<vertex x="0.675" y="-2.075"/>
<vertex x="0.825" y="-2.075"/>
<vertex x="0.825" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.15" y="-2.55"/>
<vertex x="1.15" y="-2.05"/>
<vertex x="1.35" y="-2.05"/>
<vertex x="1.35" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.175" y="-2.525"/>
<vertex x="1.175" y="-2.075"/>
<vertex x="1.325" y="-2.075"/>
<vertex x="1.325" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.85" y="-2.55"/>
<vertex x="1.85" y="-2.1"/>
<vertex x="1.8" y="-2.05"/>
<vertex x="1.65" y="-2.05"/>
<vertex x="1.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.825" y="-2.525"/>
<vertex x="1.825" y="-2.125"/>
<vertex x="1.775" y="-2.075"/>
<vertex x="1.675" y="-2.075"/>
<vertex x="1.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-1.85"/>
<vertex x="2.1" y="-1.85"/>
<vertex x="2.05" y="-1.8"/>
<vertex x="2.05" y="-1.65"/>
<vertex x="2.55" y="-1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-1.825"/>
<vertex x="2.125" y="-1.825"/>
<vertex x="2.075" y="-1.775"/>
<vertex x="2.075" y="-1.675"/>
<vertex x="2.525" y="-1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-1.35"/>
<vertex x="2.05" y="-1.35"/>
<vertex x="2.05" y="-1.15"/>
<vertex x="2.55" y="-1.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-1.325"/>
<vertex x="2.075" y="-1.325"/>
<vertex x="2.075" y="-1.175"/>
<vertex x="2.525" y="-1.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-0.85"/>
<vertex x="2.05" y="-0.85"/>
<vertex x="2.05" y="-0.65"/>
<vertex x="2.55" y="-0.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-0.825"/>
<vertex x="2.075" y="-0.825"/>
<vertex x="2.075" y="-0.675"/>
<vertex x="2.525" y="-0.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-0.35"/>
<vertex x="2.05" y="-0.35"/>
<vertex x="2.05" y="-0.15"/>
<vertex x="2.55" y="-0.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-0.325"/>
<vertex x="2.075" y="-0.325"/>
<vertex x="2.075" y="-0.175"/>
<vertex x="2.525" y="-0.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="0.15"/>
<vertex x="2.05" y="0.15"/>
<vertex x="2.05" y="0.35"/>
<vertex x="2.55" y="0.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="0.175"/>
<vertex x="2.075" y="0.175"/>
<vertex x="2.075" y="0.325"/>
<vertex x="2.525" y="0.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="0.65"/>
<vertex x="2.05" y="0.65"/>
<vertex x="2.05" y="0.85"/>
<vertex x="2.55" y="0.85"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="0.675"/>
<vertex x="2.075" y="0.675"/>
<vertex x="2.075" y="0.825"/>
<vertex x="2.525" y="0.825"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="1.15"/>
<vertex x="2.05" y="1.15"/>
<vertex x="2.05" y="1.35"/>
<vertex x="2.55" y="1.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="1.175"/>
<vertex x="2.075" y="1.175"/>
<vertex x="2.075" y="1.325"/>
<vertex x="2.525" y="1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="1.85"/>
<vertex x="2.1" y="1.85"/>
<vertex x="2.05" y="1.8"/>
<vertex x="2.05" y="1.65"/>
<vertex x="2.55" y="1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="1.825"/>
<vertex x="2.125" y="1.825"/>
<vertex x="2.075" y="1.775"/>
<vertex x="2.075" y="1.675"/>
<vertex x="2.525" y="1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.85" y="2.55"/>
<vertex x="1.85" y="2.1"/>
<vertex x="1.8" y="2.05"/>
<vertex x="1.65" y="2.05"/>
<vertex x="1.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.825" y="2.525"/>
<vertex x="1.825" y="2.125"/>
<vertex x="1.775" y="2.075"/>
<vertex x="1.675" y="2.075"/>
<vertex x="1.675" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.35" y="2.55"/>
<vertex x="1.35" y="2.05"/>
<vertex x="1.15" y="2.05"/>
<vertex x="1.15" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.325" y="2.525"/>
<vertex x="1.325" y="2.075"/>
<vertex x="1.175" y="2.075"/>
<vertex x="1.175" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.85" y="2.55"/>
<vertex x="0.85" y="2.05"/>
<vertex x="0.65" y="2.05"/>
<vertex x="0.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.825" y="2.525"/>
<vertex x="0.825" y="2.075"/>
<vertex x="0.675" y="2.075"/>
<vertex x="0.675" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.35" y="2.55"/>
<vertex x="0.35" y="2.05"/>
<vertex x="0.15" y="2.05"/>
<vertex x="0.15" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.325" y="2.525"/>
<vertex x="0.325" y="2.075"/>
<vertex x="0.175" y="2.075"/>
<vertex x="0.175" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.15" y="2.55"/>
<vertex x="-0.15" y="2.05"/>
<vertex x="-0.35" y="2.05"/>
<vertex x="-0.35" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.175" y="2.525"/>
<vertex x="-0.175" y="2.075"/>
<vertex x="-0.325" y="2.075"/>
<vertex x="-0.325" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.65" y="2.55"/>
<vertex x="-0.65" y="2.05"/>
<vertex x="-0.85" y="2.05"/>
<vertex x="-0.85" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.675" y="2.525"/>
<vertex x="-0.675" y="2.075"/>
<vertex x="-0.825" y="2.075"/>
<vertex x="-0.825" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.15" y="2.55"/>
<vertex x="-1.15" y="2.05"/>
<vertex x="-1.35" y="2.05"/>
<vertex x="-1.35" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.175" y="2.525"/>
<vertex x="-1.175" y="2.075"/>
<vertex x="-1.325" y="2.075"/>
<vertex x="-1.325" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.85" y="2.55"/>
<vertex x="-1.85" y="2.1"/>
<vertex x="-1.8" y="2.05"/>
<vertex x="-1.65" y="2.05"/>
<vertex x="-1.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.825" y="2.525"/>
<vertex x="-1.825" y="2.125"/>
<vertex x="-1.775" y="2.075"/>
<vertex x="-1.675" y="2.075"/>
<vertex x="-1.675" y="2.525"/>
</polygon>
</package>
<package name="DPAK-5">
<description>DPAK 5, center lead crop, case 175AA</description>
<wire x1="3.26" y1="-3.13" x2="-3.24" y2="-3.13" width="0.127" layer="51"/>
<wire x1="3.26" y1="-3.13" x2="3.26" y2="2.87" width="0.127" layer="51"/>
<wire x1="3.26" y1="2.87" x2="-2.21" y2="2.87" width="0.127" layer="51"/>
<wire x1="-2.21" y1="2.87" x2="-3.24" y2="2.87" width="0.127" layer="51"/>
<wire x1="-3.24" y1="2.87" x2="-3.24" y2="-3.13" width="0.127" layer="51"/>
<wire x1="-2.21" y1="2.87" x2="-2.21" y2="3.6" width="0.127" layer="51"/>
<wire x1="-2.21" y1="3.6" x2="-1.96" y2="3.85" width="0.127" layer="51"/>
<wire x1="2.29" y1="2.88" x2="2.29" y2="3.6" width="0.127" layer="51"/>
<wire x1="2.29" y1="3.6" x2="2.04" y2="3.85" width="0.127" layer="51"/>
<wire x1="2.04" y1="3.85" x2="-1.96" y2="3.85" width="0.127" layer="51"/>
<wire x1="-3.09" y1="3.03" x2="-3.48" y2="3.03" width="0.127" layer="21"/>
<wire x1="-3.48" y1="3.03" x2="-3.48" y2="-3.38" width="0.127" layer="21"/>
<wire x1="-3.48" y1="-3.38" x2="-3" y2="-3.38" width="0.127" layer="21"/>
<wire x1="-3.48" y1="3.03" x2="-3" y2="3.03" width="0.127" layer="21"/>
<wire x1="3.07" y1="-3.38" x2="3.46" y2="-3.38" width="0.127" layer="21"/>
<wire x1="3.46" y1="-3.38" x2="3.46" y2="3.03" width="0.127" layer="21"/>
<wire x1="3.46" y1="3.03" x2="2.98" y2="3.03" width="0.127" layer="21"/>
<wire x1="3.46" y1="-3.38" x2="2.98" y2="-3.38" width="0.127" layer="21"/>
<circle x="-2.27" y="-3.5" radius="0.1" width="0.254" layer="21"/>
<circle x="-1.16" y="-3.5" radius="0.1" width="0.254" layer="21"/>
<circle x="1.12" y="-3.5" radius="0.1" width="0.254" layer="21"/>
<circle x="2.23" y="-3.5" radius="0.1" width="0.254" layer="21"/>
<smd name="3" x="0" y="1" dx="6.4" dy="5.8" layer="1" rot="R90"/>
<smd name="4" x="1.14" y="-5.3" dx="2.2" dy="0.8" layer="1" rot="R90"/>
<smd name="5" x="2.28" y="-5.3" dx="2.2" dy="0.8" layer="1" rot="R90"/>
<smd name="1" x="-2.28" y="-5.3" dx="2.2" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="-1.14" y="-5.3" dx="2.2" dy="0.8" layer="1" rot="R90"/>
<text x="-3" y="4.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3" y="-3" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.62" y1="-5.8" x2="-1.92" y2="-3.1" layer="51"/>
<rectangle x1="-1.49" y1="-5.8" x2="-0.79" y2="-3.1" layer="51"/>
<rectangle x1="0.79" y1="-5.79" x2="1.49" y2="-3.09" layer="51"/>
<rectangle x1="1.92" y1="-5.79" x2="2.62" y2="-3.09" layer="51"/>
</package>
<package name="DFN-POWERFLAT-3X3">
<smd name="P$1" x="-0.975" y="-1.4" dx="0.4" dy="0.7" layer="1"/>
<smd name="P$2" x="-0.325" y="-1.4" dx="0.4" dy="0.7" layer="1"/>
<smd name="P$3" x="0.325" y="-1.4" dx="0.4" dy="0.7" layer="1"/>
<smd name="P$4" x="0.975" y="-1.4" dx="0.4" dy="0.7" layer="1"/>
<smd name="P$5" x="0" y="0.54" dx="3.1" dy="2.25" layer="1"/>
<circle x="-1.7" y="-1.9" radius="0.15" width="0.127" layer="21"/>
</package>
<package name="POWERFLAT-5X6">
<smd name="P$1" x="-1.905" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$2" x="-0.635" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$3" x="0.635" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$4" x="1.905" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$8" x="-1.45" y="0.9" dx="2.5" dy="4.35" layer="1"/>
<smd name="P$5" x="1.45" y="0.9" dx="2.5" dy="4.35" layer="1"/>
<circle x="-2.7" y="-3.8" radius="0.22360625" width="0.127" layer="21"/>
</package>
<package name="PWRPAD_SC-02_2-45MM">
<pad name="P$1" x="0" y="0" drill="2.45" diameter="4.24" thermals="no"/>
</package>
<package name="PWRPAD_4MM">
<pad name="P$1" x="0" y="0" drill="3.9878" diameter="6.35" thermals="no"/>
</package>
<package name="PWRPAD_3-25MM">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.75" thermals="no"/>
</package>
<package name="PWRPAD_2-65MM">
<pad name="P$1" x="0" y="0" drill="2.65" diameter="4.65" thermals="no"/>
</package>
<package name="PWRPAD_2-05MM">
<pad name="P$1" x="0" y="0" drill="2.05" diameter="3.8" thermals="no"/>
</package>
<package name="PWRPAD_M3_STANDOFF">
<pad name="P$1" x="0" y="0" drill="4.4" diameter="7" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3.6"/>
<vertex x="0.6" y="3.6"/>
<vertex x="0.4" y="2.1"/>
<vertex x="-0.4" y="2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3.6"/>
<vertex x="-0.6" y="-3.6"/>
<vertex x="-0.4" y="-2.1"/>
<vertex x="0.4" y="-2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.6" y="-0.6"/>
<vertex x="-3.6" y="0.6"/>
<vertex x="-2.1" y="0.4"/>
<vertex x="-2.1" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3.6" y="0.6"/>
<vertex x="3.6" y="-0.6"/>
<vertex x="2.1" y="-0.4"/>
<vertex x="2.1" y="0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.95269375" y="2.13136875"/>
<vertex x="-2.104165625" y="2.979896875"/>
<vertex x="-1.19203125" y="1.784921875"/>
<vertex x="-1.75771875" y="1.2192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.99705625" y="-2.12131875"/>
<vertex x="2.148528125" y="-2.969846875"/>
<vertex x="1.23639375" y="-1.774871875"/>
<vertex x="1.80208125" y="-1.2091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.104165625" y="-2.969846875"/>
<vertex x="-2.95269375" y="-2.12131875"/>
<vertex x="-1.75771875" y="-1.2091875"/>
<vertex x="-1.19203125" y="-1.774871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.148528125" y="2.979896875"/>
<vertex x="2.99705625" y="2.13136875"/>
<vertex x="1.80208125" y="1.2192375"/>
<vertex x="1.23639375" y="1.784921875"/>
</polygon>
<circle x="0" y="0" radius="3.5" width="0.125" layer="51"/>
</package>
<package name="PWRPAD_3-25MM-SKINNY">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.35" thermals="no"/>
</package>
<package name="PWRPAD_M25_STANDOFF">
<pad name="P$1" x="0" y="0" drill="3.7" diameter="6" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3"/>
<vertex x="0.6" y="3"/>
<vertex x="0.4" y="1.9"/>
<vertex x="-0.4" y="1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3"/>
<vertex x="-0.6" y="-3"/>
<vertex x="-0.4" y="-1.9"/>
<vertex x="0.4" y="-1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="-0.6"/>
<vertex x="-3" y="0.6"/>
<vertex x="-1.9" y="0.4"/>
<vertex x="-1.9" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3" y="0.5"/>
<vertex x="3" y="-0.7"/>
<vertex x="1.9" y="-0.5"/>
<vertex x="1.9" y="0.3"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.55269375" y="1.73136875"/>
<vertex x="-1.704165625" y="2.579896875"/>
<vertex x="-0.99203125" y="1.584921875"/>
<vertex x="-1.55771875" y="1.0192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.49705625" y="-1.72131875"/>
<vertex x="1.648528125" y="-2.569846875"/>
<vertex x="1.03639375" y="-1.574871875"/>
<vertex x="1.60208125" y="-1.0091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-1.704165625" y="-2.669846875"/>
<vertex x="-2.55269375" y="-1.82131875"/>
<vertex x="-1.55771875" y="-1.1091875"/>
<vertex x="-0.99203125" y="-1.674871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.748528125" y="2.579896875"/>
<vertex x="2.59705625" y="1.73136875"/>
<vertex x="1.60208125" y="1.0192375"/>
<vertex x="1.03639375" y="1.584921875"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="TMC262">
<pin name="VHS" x="20.32" y="40.64" length="middle" rot="R180"/>
<pin name="VS" x="20.32" y="45.72" length="middle" rot="R180"/>
<pin name="HA1" x="20.32" y="27.94" length="middle" rot="R180"/>
<pin name="HA2" x="20.32" y="25.4" length="middle" rot="R180"/>
<pin name="BMA1" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="BMA2" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="LA2" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="LA1" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="SRA" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="5VOUT" x="20.32" y="35.56" length="middle" rot="R180"/>
<pin name="TEST_ANA" x="-20.32" y="0" length="middle"/>
<pin name="VCC_IO" x="-20.32" y="45.72" length="middle"/>
<pin name="CLK" x="-20.32" y="40.64" length="middle"/>
<pin name="STEP" x="-20.32" y="35.56" length="middle"/>
<pin name="DIR" x="-20.32" y="33.02" length="middle"/>
<pin name="TST_MODE" x="-20.32" y="27.94" length="middle"/>
<pin name="ENABLE" x="-20.32" y="22.86" length="middle"/>
<pin name="CSN" x="-20.32" y="17.78" length="middle"/>
<pin name="SCK" x="-20.32" y="15.24" length="middle"/>
<pin name="SDI" x="-20.32" y="12.7" length="middle"/>
<pin name="SDO" x="-20.32" y="10.16" length="middle"/>
<pin name="SG_TST" x="-20.32" y="5.08" length="middle"/>
<pin name="HB1" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="HB2" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="BMB1" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="BMB2" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="LB2" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="LB1" x="20.32" y="-22.86" length="middle" rot="R180"/>
<pin name="SRB" x="20.32" y="-27.94" length="middle" rot="R180"/>
<pin name="GND@2" x="20.32" y="-38.1" length="middle" rot="R180"/>
<pin name="DIE_PAD" x="20.32" y="-40.64" length="middle" rot="R180"/>
<pin name="GND@1" x="20.32" y="-35.56" length="middle" rot="R180"/>
<wire x1="-15.24" y1="48.26" x2="-15.24" y2="-43.18" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-43.18" x2="15.24" y2="-43.18" width="0.254" layer="94"/>
<wire x1="15.24" y1="-43.18" x2="15.24" y2="48.26" width="0.254" layer="94"/>
<wire x1="15.24" y1="48.26" x2="-15.24" y2="48.26" width="0.254" layer="94"/>
<text x="-2.54" y="50.8" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-48.26" size="1.27" layer="96">&gt;VALUE</text>
<pin name="GNDP" x="20.32" y="-33.02" length="middle" rot="R180"/>
</symbol>
<symbol name="PNPAIR">
<pin name="GN" x="-12.7" y="-10.16" length="middle"/>
<pin name="GP" x="-12.7" y="7.62" length="middle"/>
<pin name="DN/DP" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="SN" x="5.08" y="-22.86" length="middle" rot="R90"/>
<pin name="SP" x="5.08" y="20.32" length="middle" rot="R270"/>
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="-17.78" width="0.127" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="17.78" y2="-17.78" width="0.127" layer="94"/>
<wire x1="17.78" y1="-17.78" x2="17.78" y2="15.24" width="0.127" layer="94"/>
<wire x1="17.78" y1="15.24" x2="-7.62" y2="15.24" width="0.127" layer="94"/>
<wire x1="-1.27" y1="-8.89" x2="4.445" y2="-8.89" width="0.127" layer="94"/>
<wire x1="4.445" y1="-8.89" x2="4.445" y2="-5.08" width="0.127" layer="94"/>
<wire x1="6.35" y1="-8.89" x2="5.08" y2="-8.89" width="0.127" layer="94"/>
<wire x1="5.08" y1="-8.89" x2="5.08" y2="-6.985" width="0.127" layer="94"/>
<wire x1="5.08" y1="-6.985" x2="5.08" y2="-5.08" width="0.127" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="6.35" y2="-5.08" width="0.127" layer="94"/>
<wire x1="6.35" y1="-5.08" x2="6.35" y2="-4.445" width="0.127" layer="94"/>
<wire x1="6.35" y1="-4.445" x2="6.35" y2="0" width="0.127" layer="94"/>
<wire x1="6.35" y1="0" x2="8.89" y2="0" width="0.127" layer="94"/>
<wire x1="6.35" y1="-8.89" x2="6.35" y2="-9.525" width="0.127" layer="94"/>
<wire x1="6.35" y1="-9.525" x2="6.35" y2="-11.43" width="0.127" layer="94"/>
<wire x1="6.35" y1="-8.89" x2="6.35" y2="-6.985" width="0.127" layer="94"/>
<wire x1="6.35" y1="-6.985" x2="5.08" y2="-6.985" width="0.127" layer="94"/>
<wire x1="5.08" y1="-6.985" x2="5.715" y2="-6.35" width="0.127" layer="94"/>
<wire x1="5.715" y1="-6.35" x2="5.715" y2="-7.62" width="0.127" layer="94"/>
<wire x1="5.715" y1="-7.62" x2="5.08" y2="-6.985" width="0.127" layer="94"/>
<wire x1="6.35" y1="-9.525" x2="8.255" y2="-9.525" width="0.127" layer="94"/>
<wire x1="8.255" y1="-9.525" x2="8.255" y2="-6.985" width="0.127" layer="94"/>
<wire x1="8.255" y1="-6.985" x2="8.255" y2="-6.35" width="0.127" layer="94"/>
<wire x1="8.255" y1="-6.35" x2="8.255" y2="-4.445" width="0.127" layer="94"/>
<wire x1="8.255" y1="-4.445" x2="6.35" y2="-4.445" width="0.127" layer="94"/>
<wire x1="7.62" y1="-6.35" x2="8.255" y2="-6.35" width="0.127" layer="94"/>
<wire x1="8.255" y1="-6.35" x2="8.89" y2="-6.35" width="0.127" layer="94"/>
<wire x1="7.62" y1="-6.985" x2="8.255" y2="-6.35" width="0.127" layer="94"/>
<wire x1="8.255" y1="-6.35" x2="8.89" y2="-6.985" width="0.127" layer="94"/>
<wire x1="8.89" y1="-6.985" x2="8.255" y2="-6.985" width="0.127" layer="94"/>
<wire x1="8.255" y1="-6.985" x2="7.62" y2="-6.985" width="0.127" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="4.445" y2="5.08" width="0.127" layer="94"/>
<wire x1="4.445" y1="5.08" x2="4.445" y2="8.89" width="0.127" layer="94"/>
<wire x1="6.35" y1="5.08" x2="5.08" y2="5.08" width="0.127" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="6.985" width="0.127" layer="94"/>
<wire x1="5.08" y1="6.985" x2="5.08" y2="8.89" width="0.127" layer="94"/>
<wire x1="5.08" y1="8.89" x2="6.35" y2="8.89" width="0.127" layer="94"/>
<wire x1="6.35" y1="8.89" x2="6.35" y2="9.525" width="0.127" layer="94"/>
<wire x1="6.35" y1="5.08" x2="6.35" y2="4.445" width="0.127" layer="94"/>
<wire x1="6.35" y1="5.08" x2="6.35" y2="6.985" width="0.127" layer="94"/>
<wire x1="6.35" y1="6.985" x2="5.08" y2="6.985" width="0.127" layer="94"/>
<wire x1="6.35" y1="4.445" x2="8.255" y2="4.445" width="0.127" layer="94"/>
<wire x1="8.255" y1="4.445" x2="8.255" y2="6.985" width="0.127" layer="94"/>
<wire x1="8.255" y1="6.985" x2="8.255" y2="9.525" width="0.127" layer="94"/>
<wire x1="8.255" y1="9.525" x2="6.35" y2="9.525" width="0.127" layer="94"/>
<wire x1="6.35" y1="9.525" x2="6.35" y2="12.065" width="0.127" layer="94"/>
<wire x1="8.255" y1="6.985" x2="7.62" y2="7.62" width="0.127" layer="94"/>
<wire x1="7.62" y1="7.62" x2="8.89" y2="7.62" width="0.127" layer="94"/>
<wire x1="8.89" y1="7.62" x2="8.255" y2="6.985" width="0.127" layer="94"/>
<wire x1="8.255" y1="6.985" x2="7.62" y2="6.985" width="0.127" layer="94"/>
<wire x1="8.255" y1="6.985" x2="8.89" y2="6.985" width="0.127" layer="94"/>
<wire x1="6.35" y1="6.985" x2="5.715" y2="7.62" width="0.127" layer="94"/>
<wire x1="5.715" y1="7.62" x2="5.715" y2="6.35" width="0.127" layer="94"/>
<wire x1="5.715" y1="6.35" x2="6.35" y2="6.985" width="0.127" layer="94"/>
<wire x1="6.35" y1="4.445" x2="6.35" y2="0" width="0.127" layer="94"/>
</symbol>
<symbol name="PWRPAD">
<pin name="PWRPAD" x="-5.08" y="0" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TMC262" prefix="U">
<gates>
<gate name="G$1" symbol="TMC262" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN32">
<connects>
<connect gate="G$1" pin="5VOUT" pad="9"/>
<connect gate="G$1" pin="BMA1" pad="5"/>
<connect gate="G$1" pin="BMA2" pad="4"/>
<connect gate="G$1" pin="BMB1" pad="20"/>
<connect gate="G$1" pin="BMB2" pad="21"/>
<connect gate="G$1" pin="CLK" pad="16"/>
<connect gate="G$1" pin="CSN" pad="14"/>
<connect gate="G$1" pin="DIE_PAD" pad="EXP"/>
<connect gate="G$1" pin="DIR" pad="30"/>
<connect gate="G$1" pin="ENABLE" pad="15"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@2" pad="13"/>
<connect gate="G$1" pin="GNDP" pad="28"/>
<connect gate="G$1" pin="HA1" pad="2"/>
<connect gate="G$1" pin="HA2" pad="3"/>
<connect gate="G$1" pin="HB1" pad="23"/>
<connect gate="G$1" pin="HB2" pad="22"/>
<connect gate="G$1" pin="LA1" pad="6"/>
<connect gate="G$1" pin="LA2" pad="7"/>
<connect gate="G$1" pin="LB1" pad="19"/>
<connect gate="G$1" pin="LB2" pad="18"/>
<connect gate="G$1" pin="SCK" pad="12"/>
<connect gate="G$1" pin="SDI" pad="11"/>
<connect gate="G$1" pin="SDO" pad="10"/>
<connect gate="G$1" pin="SG_TST" pad="27"/>
<connect gate="G$1" pin="SRA" pad="8"/>
<connect gate="G$1" pin="SRB" pad="17"/>
<connect gate="G$1" pin="STEP" pad="31"/>
<connect gate="G$1" pin="TEST_ANA" pad="26"/>
<connect gate="G$1" pin="TST_MODE" pad="32"/>
<connect gate="G$1" pin="VCC_IO" pad="29"/>
<connect gate="G$1" pin="VHS" pad="24"/>
<connect gate="G$1" pin="VS" pad="25"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PNPAIR-STL40C30H3LL-FDD8424H-AON7611" prefix="Q">
<gates>
<gate name="G$1" symbol="PNPAIR" x="0" y="0"/>
</gates>
<devices>
<device name="FD" package="DPAK-5">
<connects>
<connect gate="G$1" pin="DN/DP" pad="3"/>
<connect gate="G$1" pin="GN" pad="2"/>
<connect gate="G$1" pin="GP" pad="5"/>
<connect gate="G$1" pin="SN" pad="1"/>
<connect gate="G$1" pin="SP" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="STM" package="POWERFLAT-5X6">
<connects>
<connect gate="G$1" pin="DN/DP" pad="P$5 P$8"/>
<connect gate="G$1" pin="GN" pad="P$2"/>
<connect gate="G$1" pin="GP" pad="P$4"/>
<connect gate="G$1" pin="SN" pad="P$1"/>
<connect gate="G$1" pin="SP" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AON7611" package="DFN-POWERFLAT-3X3">
<connects>
<connect gate="G$1" pin="DN/DP" pad="P$5"/>
<connect gate="G$1" pin="GN" pad="P$2"/>
<connect gate="G$1" pin="GP" pad="P$4"/>
<connect gate="G$1" pin="SN" pad="P$1"/>
<connect gate="G$1" pin="SP" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWRPAD" prefix="J">
<gates>
<gate name="G$1" symbol="PWRPAD" x="0" y="0"/>
</gates>
<devices>
<device name="SC-02_2-45MM" package="PWRPAD_SC-02_2-45MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4MM" package="PWRPAD_4MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3" package="PWRPAD_3-25MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5" package="PWRPAD_2-65MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2" package="PWRPAD_2-05MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-STANDOFF" package="PWRPAD_M3_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-SKINNY" package="PWRPAD_3-25MM-SKINNY">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5_STANDOFF" package="PWRPAD_M25_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="borkedlabs-passives">
<packages>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="V+" urn="urn:adsk.eagle:symbol:26939/1" library_version="1">
<wire x1="0.889" y1="-1.27" x2="0" y2="0.127" width="0.254" layer="94"/>
<wire x1="0" y1="0.127" x2="-0.889" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-1.27" x2="0.889" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="V+" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V+" urn="urn:adsk.eagle:component:26966/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="V+" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<circle x="0" y="0" radius="0.4953" width="0" layer="51"/>
</package>
<package name="FIDUCIAL_RECT_1MM">
<smd name="P$1" x="0" y="0" dx="1" dy="1" layer="1"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="39"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="29"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="41"/>
</package>
<package name="TOMBSTONE">
<wire x1="-1" y1="-1" x2="-1" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.6" x2="-1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.2" x2="-1" y2="0.2" width="0.127" layer="21"/>
<wire x1="-1" y1="0.2" x2="-1" y2="0.5" width="0.127" layer="21"/>
<wire x1="-1" y1="0.5" x2="1" y2="0.5" width="0.127" layer="21" curve="-180"/>
<wire x1="1" y1="0.5" x2="1" y2="0.2" width="0.127" layer="21"/>
<wire x1="1" y1="0.2" x2="1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="1" y1="-0.2" x2="1" y2="-0.6" width="0.127" layer="21"/>
<wire x1="1" y1="-0.6" x2="1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="0.6" y2="-1" width="0.127" layer="21"/>
<wire x1="0.6" y1="-1" x2="0.2" y2="-1" width="0.127" layer="21"/>
<wire x1="0.2" y1="-1" x2="-0.2" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.2" y1="-1" x2="-0.6" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-1" y="-1"/>
<vertex x="1" y="-1"/>
<vertex x="1" y="0.5" curve="90"/>
<vertex x="0" y="1.5" curve="90"/>
<vertex x="-1" y="0.5"/>
</polygon>
</package>
<package name="CBA-SILK-LOGO">
<circle x="0" y="0" radius="0.254" width="0.127" layer="21"/>
<circle x="-0.762" y="0.762" radius="0.254" width="0.127" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="1.016" x2="0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.508" x2="-0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.254" x2="-1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.508" x2="1.016" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="1.016" x2="0.508" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="1.016" x2="0.508" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.254" x2="1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.254" x2="1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.254" x2="0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.508" x2="1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="0.508" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="-1.016" x2="0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.508" x2="-1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="-0.508" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="ATK-BFPP-CYCLOPS">
<pad name="P$GND@2" x="0" y="16.5" drill="4.4" diameter="6.4" rot="R270" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="3" y="16.7"/>
<vertex x="2.2" y="16.7" curve="90"/>
<vertex x="0.2" y="18.7"/>
<vertex x="0.2" y="19.5" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-0.2" y="19.5"/>
<vertex x="-0.2" y="18.7" curve="90"/>
<vertex x="-2.2" y="16.7"/>
<vertex x="-3" y="16.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="16.3"/>
<vertex x="-2.2" y="16.3" curve="90"/>
<vertex x="-0.2" y="14.3"/>
<vertex x="-0.2" y="13.5" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.2" y="13.5"/>
<vertex x="0.2" y="14.3" curve="90"/>
<vertex x="2.2" y="16.3"/>
<vertex x="3" y="16.3" curve="-90"/>
</polygon>
<wire x1="-4" y1="20" x2="36" y2="20" width="0.127" layer="47"/>
<wire x1="-4" y1="-20" x2="5.5" y2="-20" width="0.127" layer="47"/>
<hole x="-1.215" y="11.43" drill="1.2"/>
<hole x="-1.215" y="8.89" drill="1.2"/>
<hole x="-1.215" y="6.35" drill="1.2"/>
<hole x="-1.215" y="3.81" drill="1.2"/>
<hole x="-1.215" y="1.27" drill="1.2"/>
<hole x="-1.215" y="-1.27" drill="1.2"/>
<hole x="-1.215" y="-3.81" drill="1.2"/>
<hole x="-1.215" y="-6.35" drill="1.2"/>
<hole x="-1.215" y="-8.89" drill="1.2"/>
<hole x="-1.215" y="-11.43" drill="1.2"/>
<smd name="P$A1" x="-2.815" y="11.43" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$A2" x="-2.815" y="8.89" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$A3" x="-2.815" y="6.35" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$A4" x="-2.815" y="3.81" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$A5" x="-2.815" y="1.27" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$A6" x="-2.815" y="-1.27" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$A7" x="-2.815" y="-3.81" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$A8" x="-2.815" y="-6.35" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$A9" x="-2.815" y="-8.89" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$A10" x="-2.815" y="-11.43" dx="1.57" dy="1.27" layer="1"/>
<hole x="1.325" y="11.43" drill="1.2"/>
<smd name="P$B1" x="2.925" y="11.43" dx="1.57" dy="1.27" layer="1"/>
<hole x="1.325" y="8.89" drill="1.2"/>
<hole x="1.325" y="6.35" drill="1.2"/>
<hole x="1.325" y="3.81" drill="1.2"/>
<hole x="1.325" y="1.27" drill="1.2"/>
<hole x="1.325" y="-1.27" drill="1.2"/>
<hole x="1.325" y="-3.81" drill="1.2"/>
<hole x="1.325" y="-6.35" drill="1.2"/>
<hole x="1.325" y="-8.89" drill="1.2"/>
<hole x="1.325" y="-11.43" drill="1.2"/>
<smd name="P$B2" x="2.925" y="8.89" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$B3" x="2.925" y="6.35" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$B4" x="2.925" y="3.81" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$B5" x="2.925" y="1.27" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$B6" x="2.925" y="-1.27" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$B7" x="2.925" y="-3.81" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$B8" x="2.925" y="-6.35" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$B9" x="2.925" y="-8.89" dx="1.57" dy="1.27" layer="1"/>
<smd name="P$B10" x="2.925" y="-11.43" dx="1.57" dy="1.27" layer="1"/>
<dimension x1="-6" y1="16.5" x2="-6" y2="0" x3="-9" y3="8.25" textsize="1.27" layer="47"/>
<dimension x1="-6" y1="0" x2="-6" y2="-16.5" x3="-9" y3="-8.25" textsize="1.27" layer="47"/>
<pad name="P$GND@1" x="0" y="-16.5" drill="4.4" diameter="6.4" rot="R270" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="3" y="-16.3"/>
<vertex x="2.2" y="-16.3" curve="90"/>
<vertex x="0.2" y="-14.3"/>
<vertex x="0.2" y="-13.5" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-0.2" y="-13.5"/>
<vertex x="-0.2" y="-14.3" curve="90"/>
<vertex x="-2.2" y="-16.3"/>
<vertex x="-3" y="-16.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="-16.7"/>
<vertex x="-2.2" y="-16.7" curve="90"/>
<vertex x="-0.2" y="-18.7"/>
<vertex x="-0.2" y="-19.5" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.2" y="-19.5"/>
<vertex x="0.2" y="-18.7" curve="90"/>
<vertex x="2.2" y="-16.7"/>
<vertex x="3" y="-16.7" curve="-90"/>
</polygon>
<dimension x1="-6" y1="-20" x2="-6" y2="20" x3="-14" y3="0" textsize="1.27" layer="47"/>
<wire x1="5.5" y1="-20" x2="29.5" y2="-20" width="0.127" layer="47"/>
<wire x1="29.5" y1="-20" x2="36" y2="-20" width="0.127" layer="47"/>
<wire x1="-4" y1="-20" x2="-4" y2="20" width="0.127" layer="47"/>
<wire x1="-2.485" y1="-12.7" x2="2.595" y2="-12.7" width="0.127" layer="51"/>
<wire x1="2.595" y1="-12.7" x2="2.595" y2="12.7" width="0.127" layer="51"/>
<wire x1="2.595" y1="12.7" x2="-2.485" y2="12.7" width="0.127" layer="51"/>
<wire x1="-2.485" y1="12.7" x2="-2.485" y2="-12.7" width="0.127" layer="51"/>
<wire x1="36" y1="-20" x2="36" y2="20" width="0.127" layer="47"/>
<text x="-2.485" y="11.43" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">5v</text>
<text x="2.595" y="11.43" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">3v3</text>
<text x="-2.485" y="8.89" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">A04</text>
<text x="2.595" y="8.89" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">A05</text>
<text x="-2.485" y="6.35" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">A06</text>
<text x="2.595" y="6.35" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">A07</text>
<text x="-2.485" y="3.81" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">GND</text>
<text x="2.595" y="3.81" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">GND</text>
<text x="-2.485" y="1.27" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">B10</text>
<text x="2.595" y="1.27" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">A08</text>
<text x="-2.485" y="-1.27" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">B11</text>
<text x="2.595" y="-1.27" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">A09</text>
<text x="-2.485" y="-3.81" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">B12</text>
<text x="2.595" y="-3.81" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">B13</text>
<text x="-2.485" y="-6.35" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">B14</text>
<text x="2.595" y="-6.35" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">B15</text>
<text x="-2.485" y="-8.89" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">A16</text>
<text x="2.595" y="-8.89" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">A17</text>
<text x="-2.485" y="-11.43" size="0.6096" layer="22" font="vector" rot="MR0" align="center-left">A18</text>
<text x="2.595" y="-11.43" size="0.6096" layer="22" font="vector" rot="MR0" align="center-right">A19</text>
<wire x1="31.381" y1="10.318" x2="25.581" y2="10.318" width="0.127" layer="47"/>
<wire x1="25.581" y1="10.318" x2="25.581" y2="0.518" width="0.127" layer="47"/>
<wire x1="25.581" y1="0.518" x2="31.381" y2="0.518" width="0.127" layer="47"/>
<wire x1="31.381" y1="0.518" x2="31.381" y2="10.318" width="0.127" layer="47"/>
<circle x="29.481" y="7.918" radius="0.4" width="0.127" layer="47"/>
<circle x="27.481" y="7.918" radius="0.4" width="0.127" layer="47"/>
<circle x="28.481" y="2.818" radius="0.4" width="0.127" layer="47"/>
<wire x1="5.5" y1="-20" x2="5.5" y2="-6" width="0.127" layer="47"/>
<wire x1="5.5" y1="-6" x2="29.5" y2="-6" width="0.127" layer="47"/>
<wire x1="29.5" y1="-6" x2="29.5" y2="-20" width="0.127" layer="47"/>
<wire x1="5.5" y1="15.5" x2="5.5" y2="21" width="0.127" layer="47"/>
<wire x1="5.5" y1="21" x2="13.5" y2="21" width="0.127" layer="47"/>
<wire x1="13.5" y1="21" x2="13.5" y2="15.5" width="0.127" layer="47"/>
<wire x1="13.5" y1="15.5" x2="5.5" y2="15.5" width="0.127" layer="47"/>
<wire x1="14.79" y1="19.35" x2="17.29" y2="19.35" width="0.127" layer="47"/>
<wire x1="17.29" y1="19.35" x2="17.29" y2="18.35" width="0.127" layer="47"/>
<wire x1="17.29" y1="18.35" x2="17.29" y2="15.85" width="0.127" layer="47"/>
<wire x1="17.29" y1="15.85" x2="17.29" y2="14.85" width="0.127" layer="47"/>
<wire x1="17.29" y1="14.85" x2="14.79" y2="14.85" width="0.127" layer="47"/>
<wire x1="14.79" y1="14.85" x2="14.79" y2="15.85" width="0.127" layer="47"/>
<wire x1="14.79" y1="15.85" x2="14.79" y2="18.35" width="0.127" layer="47"/>
<wire x1="14.79" y1="19.35" x2="14.79" y2="18.35" width="0.127" layer="47"/>
<wire x1="14.79" y1="18.35" x2="17.29" y2="15.85" width="0.127" layer="47"/>
<wire x1="17.29" y1="15.85" x2="14.79" y2="15.85" width="0.127" layer="47"/>
<wire x1="14.79" y1="15.85" x2="17.29" y2="18.35" width="0.127" layer="47"/>
<wire x1="17.29" y1="18.35" x2="14.79" y2="18.35" width="0.127" layer="47"/>
<dimension x1="0" y1="22" x2="-4" y2="22" x3="-2" y3="27" textsize="1.27" layer="47"/>
<dimension x1="-4" y1="-22" x2="36" y2="-22" x3="16" y3="-27" textsize="1.27" layer="47"/>
<wire x1="5.5" y1="-20" x2="5.5" y2="-21" width="0.127" layer="51"/>
<wire x1="5.5" y1="-21" x2="29.5" y2="-21" width="0.127" layer="47"/>
<wire x1="29.5" y1="-21" x2="29.5" y2="-20" width="0.127" layer="47"/>
</package>
<package name="JST-6-SMD-HORI-1.0MM">
<description>&lt;h3&gt;JST SH Vertical 6-Pin SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch: 1 mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/GPS/EM406-SMDConnector-eSH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;JST_6PIN_VERTICAL&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="-3.44" y="-5.16" radius="0.1047" width="0.4064" layer="21"/>
<wire x1="-2.9" y1="-0.1" x2="2.9" y2="-0.1" width="0.254" layer="21"/>
<wire x1="-4" y1="-2.1" x2="-4" y2="-4.4" width="0.254" layer="21"/>
<wire x1="3.1" y1="-4.4" x2="4" y2="-4.4" width="0.254" layer="21"/>
<wire x1="4" y1="-4.4" x2="4" y2="-2.1" width="0.254" layer="21"/>
<wire x1="-4" y1="-4.4" x2="-3.1" y2="-4.4" width="0.254" layer="21"/>
<smd name="1" x="-2.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-1.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-0.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="0.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="2.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="M1" x="-3.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<smd name="M2" x="3.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<text x="-1.524" y="0.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.905" y="-6.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="LITE-TRAP-MOLEX-203863">
<smd name="P$1" x="-3.05" y="0" dx="1" dy="1" layer="1" stop="no" thermals="no" cream="no"/>
<wire x1="-1.65" y1="1.5" x2="4.15" y2="1.5" width="0.0762" layer="51"/>
<wire x1="-1.65" y1="-1.5" x2="4.15" y2="-1.5" width="0.0762" layer="51"/>
<circle x="0" y="0" radius="1" width="0.0762" layer="51"/>
<wire x1="-3.5" y1="0" x2="-4" y2="0.5" width="0.0762" layer="51"/>
<wire x1="-4" y1="0.5" x2="-4" y2="-0.5" width="0.0762" layer="51"/>
<wire x1="-4" y1="-0.5" x2="-3.5" y2="0" width="0.0762" layer="51"/>
<polygon width="0.1" layer="1">
<vertex x="-1.9" y="1.25"/>
<vertex x="4.2" y="1.25"/>
<vertex x="4.2" y="-1.25"/>
<vertex x="-1.9" y="-1.25"/>
<vertex x="-1.9" y="-1.7"/>
<vertex x="-4.2" y="-1.7"/>
<vertex x="-4.2" y="1.7"/>
<vertex x="-1.9" y="1.7"/>
</polygon>
<polygon width="0.1" layer="31">
<vertex x="-4.2" y="1.7"/>
<vertex x="-1.9" y="1.7"/>
<vertex x="-1.9" y="1.25"/>
<vertex x="4.2" y="1.25"/>
<vertex x="4.2" y="-1.25"/>
<vertex x="-1.9" y="-1.25"/>
<vertex x="-1.9" y="-1.65"/>
<vertex x="-1.9" y="-1.7"/>
<vertex x="-4.2" y="-1.7"/>
</polygon>
<polygon width="0.1" layer="29">
<vertex x="-4.35" y="1.85"/>
<vertex x="-1.75" y="1.85"/>
<vertex x="-1.75" y="1.4"/>
<vertex x="4.35" y="1.4"/>
<vertex x="4.35" y="-1.4"/>
<vertex x="-1.75" y="-1.4"/>
<vertex x="-1.75" y="-1.85"/>
<vertex x="-4.35" y="-1.85"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="TOMBSTONE">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="CBALOGO">
<text x="0" y="0" size="1.27" layer="97" align="center">~ center for bits and atoms ~
cba.mit.edu</text>
</symbol>
<symbol name="ATK-BFPP-CYCLOPS">
<pin name="GND" x="-7.62" y="5.08" length="middle"/>
<pin name="+3V3" x="-7.62" y="-12.7" length="middle"/>
<pin name="+5V" x="-7.62" y="-20.32" length="middle"/>
<pin name="ANLG-0/SER0-0(PA04)" x="-7.62" y="-27.94" length="middle"/>
<pin name="ANLG-1/SER0-1/DAC(PA05)" x="-7.62" y="-30.48" length="middle"/>
<pin name="ANLG-2/SER0-2(PA06)" x="-7.62" y="-33.02" length="middle"/>
<pin name="ANLG-3/SER0-3(PA07)" x="-7.62" y="-35.56" length="middle"/>
<pin name="PWMALS(PA08)" x="-7.62" y="-43.18" length="middle"/>
<pin name="PWMAHS(PB10)" x="-7.62" y="-45.72" length="middle"/>
<pin name="PWMBLS(PA09)" x="-7.62" y="-48.26" length="middle"/>
<pin name="PWMBHS(PB11)" x="-7.62" y="-50.8" length="middle"/>
<pin name="SER1-0-SDA/TXD/MOSI-(PA16)" x="-7.62" y="-58.42" length="middle"/>
<pin name="SER1-1-SCL/XCL/CLK-(PA17)" x="-7.62" y="-60.96" length="middle"/>
<pin name="SER1-2-RXD/MISO-(PA18)" x="-7.62" y="-63.5" length="middle"/>
<pin name="SER1-3-CSN-(PA19)" x="-7.62" y="-66.04" length="middle"/>
<pin name="SER4-0(PB12)" x="-7.62" y="-73.66" length="middle"/>
<pin name="SER4-1(PB13)" x="-7.62" y="-76.2" length="middle"/>
<pin name="SER4-2(PB14)" x="-7.62" y="-78.74" length="middle"/>
<pin name="SER4-3(PB15)" x="-7.62" y="-81.28" length="middle"/>
<wire x1="-2.54" y1="10.16" x2="-2.54" y2="-86.36" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-86.36" x2="35.56" y2="-86.36" width="0.254" layer="94"/>
<wire x1="35.56" y1="-86.36" x2="35.56" y2="10.16" width="0.254" layer="94"/>
<wire x1="35.56" y1="10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="CONN_06">
<description>&lt;h3&gt;6 Pin Connection&lt;/h3&gt;</description>
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-5.08" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-5.08" y="10.668" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CONN_01">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="3.048" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIAL" prefix="J">
<description>For use by pick and place machines to calibrate the vision/machine, 1mm
&lt;p&gt;By microbuilder.eu&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RCT" package="FIDUCIAL_RECT_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TOMBSTONE" prefix="MP">
<gates>
<gate name="G$1" symbol="TOMBSTONE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TOMBSTONE">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CBA-LOGO">
<gates>
<gate name="G$1" symbol="CBALOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CBA-SILK-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATK-BFPP-CYCLOPS" prefix="J">
<gates>
<gate name="G$1" symbol="ATK-BFPP-CYCLOPS" x="0" y="0"/>
</gates>
<devices>
<device name="CYCLOPS" package="ATK-BFPP-CYCLOPS">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$B1"/>
<connect gate="G$1" pin="+5V" pad="P$A1"/>
<connect gate="G$1" pin="ANLG-0/SER0-0(PA04)" pad="P$A2"/>
<connect gate="G$1" pin="ANLG-1/SER0-1/DAC(PA05)" pad="P$B2"/>
<connect gate="G$1" pin="ANLG-2/SER0-2(PA06)" pad="P$A3"/>
<connect gate="G$1" pin="ANLG-3/SER0-3(PA07)" pad="P$B3"/>
<connect gate="G$1" pin="GND" pad="P$A4 P$B4 P$GND@1 P$GND@2"/>
<connect gate="G$1" pin="PWMAHS(PB10)" pad="P$A5"/>
<connect gate="G$1" pin="PWMALS(PA08)" pad="P$B5"/>
<connect gate="G$1" pin="PWMBHS(PB11)" pad="P$A6"/>
<connect gate="G$1" pin="PWMBLS(PA09)" pad="P$B6"/>
<connect gate="G$1" pin="SER1-0-SDA/TXD/MOSI-(PA16)" pad="P$A9"/>
<connect gate="G$1" pin="SER1-1-SCL/XCL/CLK-(PA17)" pad="P$B9"/>
<connect gate="G$1" pin="SER1-2-RXD/MISO-(PA18)" pad="P$A10"/>
<connect gate="G$1" pin="SER1-3-CSN-(PA19)" pad="P$B10"/>
<connect gate="G$1" pin="SER4-0(PB12)" pad="P$A7"/>
<connect gate="G$1" pin="SER4-1(PB13)" pad="P$B7"/>
<connect gate="G$1" pin="SER4-2(PB14)" pad="P$A8"/>
<connect gate="G$1" pin="SER4-3(PB15)" pad="P$B8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_6_PIN_HORIZONTAL" prefix="J">
<description>&lt;h3&gt;JST 6 pin horizontal connector&lt;/h3&gt;
JST-SH type.

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Here is the connector we sell at SparkFun:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="link"&gt;name&lt;/a&gt; (XXX-00000)&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.sparkfun.com/datasheets/GPS/EM406-SMDConnector-eSH.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;It was used on these SparkFun products:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="link"&gt;name&lt;/a&gt; (XXX-00000)&lt;/li&gt;
&lt;li&gt;&lt;a href="link"&gt;name&lt;/a&gt; (XXX-00000)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="A" symbol="CONN_06" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST-6-SMD-HORI-1.0MM">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XXX-00000" constant="no"/>
<attribute name="VALUE" value="BM06B-SRSS-TB" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POKEPOWER">
<gates>
<gate name="G$1" symbol="CONN_01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LITE-TRAP-MOLEX-203863">
<connects>
<connect gate="G$1" pin="2" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="sensor">
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT 23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1524" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.8636" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="0.762" dy="1.016" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="0.762" dy="1.016" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="0.762" dy="1.016" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="RTD-NI1000SOT">
<pin name="TERM1" x="-12.7" y="5.08" length="middle"/>
<pin name="TERM2" x="-12.7" y="-5.08" length="middle"/>
<pin name="SENSE" x="12.7" y="0" length="middle" rot="R180"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RTD-NI1000SOT" prefix="U">
<gates>
<gate name="G$1" symbol="RTD-NI1000SOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="SENSE" pad="3"/>
<connect gate="G$1" pin="TERM1" pad="1"/>
<connect gate="G$1" pin="TERM2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="SOD123">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.321" y1="0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="1.321" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="-1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="1.321" y1="-0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1" y1="0" x2="0" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="-0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="CATHODE" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<smd name="ANODE" x="1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.9558" y1="-0.3048" x2="-1.3716" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="1.3716" y1="-0.3048" x2="1.9558" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
<wire x1="-2.667" y1="0.889" x2="-2.667" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="0.889" x2="-2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-0.889" x2="2.794" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="-0.889" x2="2.794" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="0.889" x2="-2.921" y2="0.889" width="0.127" layer="21"/>
</package>
<package name="0805-DIODE">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="0.3048" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.3048" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
<package name="SOD-123HE">
<smd name="P$1" x="0.8" y="0" dx="2.4" dy="1.4" layer="1"/>
<smd name="P$2" x="-1.55" y="0" dx="0.9" dy="1.4" layer="1"/>
<wire x1="-1.4" y1="-0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="-0.9" x2="1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.9" y2="0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="-1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.9" x2="0.5" y2="-0.9" width="0.127" layer="21"/>
<text x="0" y="1.8" size="1.27" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="SMA-403D">
<smd name="P$1" x="-2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<smd name="P$2" x="2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.2" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="1.6" x2="-1.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.2" x2="-2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.6" x2="-1.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-1.6" x2="2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="1.6" x2="2" y2="1.6" width="0.127" layer="21"/>
<text x="0" y="2.4" size="1.27" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
high speed (Philips)</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIODE" package="0805-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD123HE" package="SOD-123HE">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMA403D" package="SMA-403D">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="R2" library="borkedlabs-passives" deviceset="RESISTOR" device="2512" value="50mOhm"/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="V+" device=""/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C11" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="+3V310" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R4" library="borkedlabs-passives" deviceset="RESISTOR" device="2512" value="50mOhm"/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C16" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C17" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C4" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R3" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="U2" library="power" deviceset="TMC262" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="Q2" library="power" deviceset="PNPAIR-STL40C30H3LL-FDD8424H-AON7611" device="STM" value="PNPAIR-STL40C30H3LL-FDD8424H-AON7611STM"/>
<part name="Q1" library="power" deviceset="PNPAIR-STL40C30H3LL-FDD8424H-AON7611" device="STM" value="PNPAIR-STL40C30H3LL-FDD8424H-AON7611STM"/>
<part name="Q3" library="power" deviceset="PNPAIR-STL40C30H3LL-FDD8424H-AON7611" device="STM" value="PNPAIR-STL40C30H3LL-FDD8424H-AON7611STM"/>
<part name="Q4" library="power" deviceset="PNPAIR-STL40C30H3LL-FDD8424H-AON7611" device="STM" value="PNPAIR-STL40C30H3LL-FDD8424H-AON7611STM"/>
<part name="C12" library="borkedlabs-passives" deviceset="CAP" device="0805" value="1uF"/>
<part name="C10" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF 16v"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="V+" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="V+" device=""/>
<part name="U$1" library="connector" deviceset="FIDUCIAL" device="RCT" value="FIDUCIALRCT"/>
<part name="U$3" library="connector" deviceset="FIDUCIAL" device="RCT" value="FIDUCIALRCT"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U1" library="sensor" deviceset="RTD-NI1000SOT" device=""/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R1" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="V+" device=""/>
<part name="C2" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C18" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C6" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C15" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C3" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C14" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C8" library="borkedlabs-passives" deviceset="CAP" device="0805" value="1uF"/>
<part name="C5" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C7" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C19" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="V+" device=""/>
<part name="C1" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C13" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="MP1" library="connector" deviceset="TOMBSTONE" device=""/>
<part name="U$2" library="connector" deviceset="CBA-LOGO" device=""/>
<part name="J2" library="connector" deviceset="ATK-BFPP-CYCLOPS" device="CYCLOPS"/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J1" library="connector" deviceset="JST_6_PIN_HORIZONTAL" device="" value="BM06B-SRSS-TB"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="J3" library="power" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="J4" library="power" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="D1" library="passives" deviceset="DIODE" device="SMA403D"/>
<part name="U$5" library="connector" deviceset="POKEPOWER" device=""/>
<part name="U$4" library="connector" deviceset="POKEPOWER" device=""/>
<part name="C9" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="C20" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
<part name="U$6" library="connector" deviceset="POKEPOWER" device=""/>
<part name="U$7" library="connector" deviceset="POKEPOWER" device=""/>
<part name="U$8" library="connector" deviceset="POKEPOWER" device=""/>
<part name="C21" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF 50v"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="R2" gate="G$1" x="304.8" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="303.3014" y="59.69" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="308.102" y="59.69" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="311.658" y="59.69" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="309.88" y="59.69" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="P+4" gate="1" x="411.48" y="213.36" smashed="yes" rot="R270">
<attribute name="VALUE" x="408.94" y="215.9" size="1.778" layer="96"/>
</instance>
<instance part="GND23" gate="1" x="411.48" y="205.74" smashed="yes" rot="R90">
<attribute name="VALUE" x="414.02" y="203.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="182.88" y="187.96" smashed="yes" rot="R180">
<attribute name="NAME" x="181.356" y="185.039" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="181.356" y="190.119" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="181.356" y="192.024" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="181.356" y="193.802" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="181.356" y="195.58" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="+3V310" gate="G$1" x="142.24" y="190.5" smashed="yes" rot="R90">
<attribute name="VALUE" x="147.32" y="187.96" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND13" gate="1" x="170.18" y="182.88" smashed="yes" rot="R270">
<attribute name="VALUE" x="167.64" y="185.42" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND20" gate="1" x="304.8" y="53.34" smashed="yes">
<attribute name="VALUE" x="302.26" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="396.24" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="394.7414" y="59.69" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="399.542" y="59.69" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="403.098" y="59.69" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="401.32" y="59.69" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="GND19" gate="1" x="396.24" y="53.34" smashed="yes">
<attribute name="VALUE" x="393.7" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="167.64" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="166.116" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="166.116" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="166.116" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="166.116" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="166.116" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C17" gate="G$1" x="180.34" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="178.816" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="178.816" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="178.816" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="178.816" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="178.816" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C4" gate="G$1" x="193.04" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="191.516" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="191.516" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="191.516" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="191.516" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="191.516" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND15" gate="1" x="231.14" y="96.52" smashed="yes">
<attribute name="VALUE" x="228.6" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="157.48" y="172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="155.9814" y="168.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="160.782" y="168.91" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="164.338" y="168.91" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="162.56" y="168.91" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="U2" gate="G$1" x="208.28" y="144.78" smashed="yes">
<attribute name="NAME" x="205.74" y="195.58" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="96.52" size="1.27" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="170.18" y="172.72" smashed="yes" rot="R270">
<attribute name="VALUE" x="167.64" y="175.26" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="Q2" gate="G$1" x="276.86" y="99.06" smashed="yes"/>
<instance part="Q1" gate="G$1" x="322.58" y="99.06" smashed="yes"/>
<instance part="Q3" gate="G$1" x="370.84" y="99.06" smashed="yes"/>
<instance part="Q4" gate="G$1" x="416.56" y="99.06" smashed="yes"/>
<instance part="C12" gate="G$1" x="238.76" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="235.839" y="186.944" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="240.919" y="186.944" size="1.778" layer="96" rot="R90"/>
<attribute name="PACKAGE" x="242.824" y="186.944" size="1.27" layer="97" rot="R90"/>
<attribute name="VOLTAGE" x="244.602" y="186.944" size="1.27" layer="97" rot="R90"/>
<attribute name="TYPE" x="246.38" y="186.944" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="C10" gate="G$1" x="259.08" y="185.42" smashed="yes">
<attribute name="NAME" x="260.604" y="188.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="260.604" y="183.261" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="260.604" y="181.356" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="260.604" y="179.578" size="1.27" layer="97"/>
<attribute name="TYPE" x="260.604" y="177.8" size="1.27" layer="97"/>
</instance>
<instance part="P+1" gate="1" x="279.4" y="190.5" smashed="yes" rot="R270">
<attribute name="VALUE" x="276.86" y="193.04" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="279.4" y="182.88" smashed="yes" rot="R90">
<attribute name="VALUE" x="281.94" y="180.34" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+2" gate="1" x="350.52" y="137.16" smashed="yes">
<attribute name="VALUE" x="347.98" y="134.62" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="U$3" gate="G$1" x="482.6" y="228.6" smashed="yes"/>
<instance part="GND3" gate="1" x="251.46" y="167.64" smashed="yes">
<attribute name="VALUE" x="248.92" y="165.1" size="1.778" layer="96"/>
</instance>
<instance part="U1" gate="G$1" x="200.66" y="30.48" smashed="yes">
<attribute name="NAME" x="195.58" y="40.64" size="1.778" layer="95"/>
<attribute name="VALUE" x="195.58" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="+3V5" gate="G$1" x="172.72" y="35.56" smashed="yes" rot="R90">
<attribute name="VALUE" x="177.8" y="33.02" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND12" gate="1" x="185.42" y="7.62" smashed="yes">
<attribute name="VALUE" x="182.88" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="185.42" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="183.9214" y="13.97" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="188.722" y="13.97" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="192.278" y="13.97" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="190.5" y="13.97" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="457.2" y="78.74" smashed="yes" rot="R270">
<attribute name="VALUE" x="454.66" y="81.28" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+5" gate="1" x="459.74" y="73.66" smashed="yes" rot="R180">
<attribute name="VALUE" x="462.28" y="76.2" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C2" gate="G$1" x="205.74" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="204.216" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="204.216" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="204.216" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="204.216" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="204.216" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C18" gate="G$1" x="231.14" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="229.616" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="229.616" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="229.616" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="229.616" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="229.616" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C6" gate="G$1" x="154.94" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="153.416" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="153.416" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="153.416" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="153.416" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="153.416" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C15" gate="G$1" x="243.84" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="242.316" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="242.316" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="242.316" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="242.316" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="242.316" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C3" gate="G$1" x="256.54" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="255.016" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="255.016" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="255.016" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="255.016" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="255.016" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C14" gate="G$1" x="269.24" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="267.716" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="267.716" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="267.716" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="267.716" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="267.716" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C8" gate="G$1" x="251.46" y="175.26" smashed="yes">
<attribute name="NAME" x="252.984" y="178.181" size="1.778" layer="95"/>
<attribute name="VALUE" x="252.984" y="173.101" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="252.984" y="171.196" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="252.984" y="169.418" size="1.27" layer="97"/>
<attribute name="TYPE" x="252.984" y="167.64" size="1.27" layer="97"/>
</instance>
<instance part="C5" gate="G$1" x="218.44" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="216.916" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="216.916" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="216.916" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="216.916" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="216.916" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C7" gate="G$1" x="281.94" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="280.416" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="280.416" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="280.416" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="280.416" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="280.416" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C19" gate="G$1" x="307.34" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="305.816" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="305.816" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="305.816" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="305.816" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="305.816" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="P+6" gate="1" x="218.44" y="30.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="215.9" y="33.02" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="320.04" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="318.516" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="318.516" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="318.516" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="318.516" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="318.516" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C13" gate="G$1" x="332.74" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="331.216" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="331.216" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="331.216" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="331.216" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="331.216" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="MP1" gate="G$1" x="12.7" y="5.08" smashed="yes"/>
<instance part="U$2" gate="G$1" x="12.7" y="15.24" smashed="yes"/>
<instance part="J2" gate="G$1" x="60.96" y="182.88" smashed="yes"/>
<instance part="+3V3" gate="G$1" x="30.48" y="154.94" smashed="yes" rot="R90">
<attribute name="VALUE" x="35.56" y="152.4" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+3V4" gate="G$1" x="35.56" y="170.18" smashed="yes" rot="R90">
<attribute name="VALUE" x="40.64" y="167.64" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND2" gate="1" x="35.56" y="187.96" smashed="yes" rot="R270">
<attribute name="VALUE" x="33.02" y="190.5" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J1" gate="A" x="162.56" y="76.2" smashed="yes" rot="R180">
<attribute name="VALUE" x="167.64" y="86.106" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="167.64" y="65.532" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="142.24" y="83.82" smashed="yes" rot="R180">
<attribute name="VALUE" x="144.78" y="86.36" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+3V1" gate="G$1" x="132.08" y="78.74" smashed="yes" rot="R90">
<attribute name="VALUE" x="137.16" y="76.2" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J3" gate="G$1" x="482.6" y="68.58" smashed="yes"/>
<instance part="J4" gate="G$1" x="482.6" y="63.5" smashed="yes"/>
<instance part="D1" gate="G$1" x="365.76" y="208.28" smashed="yes" rot="R90">
<attribute name="NAME" x="365.2774" y="210.82" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="368.0714" y="210.82" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$5" gate="G$1" x="388.62" y="220.98" smashed="yes" rot="R270">
<attribute name="VALUE" x="383.794" y="223.52" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="391.668" y="223.52" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="U$4" gate="G$1" x="477.52" y="111.76" smashed="yes" rot="R180">
<attribute name="VALUE" x="480.06" y="116.586" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="480.06" y="108.712" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="C9" gate="G$1" x="294.64" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="293.116" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="293.116" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="293.116" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="293.116" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="293.116" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C20" gate="G$1" x="345.44" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="343.916" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="343.916" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="343.916" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="343.916" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="343.916" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="U$6" gate="G$1" x="477.52" y="104.14" smashed="yes" rot="R180">
<attribute name="VALUE" x="480.06" y="108.966" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="480.06" y="101.092" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U$7" gate="G$1" x="477.52" y="96.52" smashed="yes" rot="R180">
<attribute name="VALUE" x="480.06" y="101.346" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="480.06" y="93.472" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="U$8" gate="G$1" x="477.52" y="88.9" smashed="yes" rot="R180">
<attribute name="VALUE" x="480.06" y="93.726" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="480.06" y="85.852" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="C21" gate="G$1" x="358.14" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="356.616" y="207.899" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="356.616" y="212.979" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="356.616" y="214.884" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="356.616" y="216.662" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="356.616" y="218.44" size="1.27" layer="97" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="408.94" y1="205.74" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<wire x1="365.76" y1="205.74" x2="358.14" y2="205.74" width="0.1524" layer="91"/>
<wire x1="358.14" y1="205.74" x2="345.44" y2="205.74" width="0.1524" layer="91"/>
<wire x1="345.44" y1="205.74" x2="332.74" y2="205.74" width="0.1524" layer="91"/>
<wire x1="332.74" y1="205.74" x2="320.04" y2="205.74" width="0.1524" layer="91"/>
<wire x1="320.04" y1="205.74" x2="307.34" y2="205.74" width="0.1524" layer="91"/>
<wire x1="307.34" y1="205.74" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="294.64" y1="205.74" x2="281.94" y2="205.74" width="0.1524" layer="91"/>
<wire x1="281.94" y1="205.74" x2="269.24" y2="205.74" width="0.1524" layer="91"/>
<wire x1="269.24" y1="205.74" x2="256.54" y2="205.74" width="0.1524" layer="91"/>
<wire x1="256.54" y1="205.74" x2="243.84" y2="205.74" width="0.1524" layer="91"/>
<wire x1="243.84" y1="205.74" x2="231.14" y2="205.74" width="0.1524" layer="91"/>
<wire x1="231.14" y1="205.74" x2="218.44" y2="205.74" width="0.1524" layer="91"/>
<wire x1="218.44" y1="205.74" x2="205.74" y2="205.74" width="0.1524" layer="91"/>
<wire x1="205.74" y1="205.74" x2="193.04" y2="205.74" width="0.1524" layer="91"/>
<wire x1="193.04" y1="205.74" x2="180.34" y2="205.74" width="0.1524" layer="91"/>
<wire x1="180.34" y1="205.74" x2="167.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="167.64" y1="205.74" x2="154.94" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<junction x="167.64" y="205.74"/>
<pinref part="C17" gate="G$1" pin="1"/>
<junction x="180.34" y="205.74"/>
<pinref part="C4" gate="G$1" pin="1"/>
<junction x="193.04" y="205.74"/>
<pinref part="C2" gate="G$1" pin="1"/>
<junction x="205.74" y="205.74"/>
<pinref part="C18" gate="G$1" pin="1"/>
<junction x="231.14" y="205.74"/>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="C15" gate="G$1" pin="1"/>
<junction x="243.84" y="205.74"/>
<pinref part="C3" gate="G$1" pin="1"/>
<junction x="256.54" y="205.74"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="269.24" y="205.74"/>
<pinref part="C5" gate="G$1" pin="1"/>
<junction x="218.44" y="205.74"/>
<pinref part="C7" gate="G$1" pin="1"/>
<junction x="281.94" y="205.74"/>
<pinref part="C19" gate="G$1" pin="1"/>
<junction x="307.34" y="205.74"/>
<pinref part="C1" gate="G$1" pin="1"/>
<junction x="320.04" y="205.74"/>
<pinref part="C13" gate="G$1" pin="1"/>
<junction x="332.74" y="205.74"/>
<pinref part="D1" gate="G$1" pin="A"/>
<junction x="365.76" y="205.74"/>
<pinref part="C9" gate="G$1" pin="1"/>
<junction x="294.64" y="205.74"/>
<pinref part="C20" gate="G$1" pin="1"/>
<junction x="345.44" y="205.74"/>
<pinref part="C21" gate="G$1" pin="1"/>
<junction x="358.14" y="205.74"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="172.72" y1="182.88" x2="182.88" y2="182.88" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CLK"/>
<wire x1="187.96" y1="185.42" x2="185.42" y2="185.42" width="0.1524" layer="91"/>
<wire x1="185.42" y1="185.42" x2="185.42" y2="182.88" width="0.1524" layer="91"/>
<wire x1="185.42" y1="182.88" x2="182.88" y2="182.88" width="0.1524" layer="91"/>
<junction x="182.88" y="182.88"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="304.8" y1="55.88" x2="304.8" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="396.24" y1="55.88" x2="396.24" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="TST_MODE"/>
<wire x1="187.96" y1="172.72" x2="172.72" y2="172.72" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GNDP"/>
<wire x1="228.6" y1="111.76" x2="231.14" y2="111.76" width="0.1524" layer="91"/>
<wire x1="231.14" y1="111.76" x2="231.14" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND@1"/>
<wire x1="231.14" y1="109.22" x2="228.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="231.14" y1="109.22" x2="231.14" y2="106.68" width="0.1524" layer="91"/>
<junction x="231.14" y="109.22"/>
<pinref part="U2" gate="G$1" pin="GND@2"/>
<wire x1="231.14" y1="106.68" x2="228.6" y2="106.68" width="0.1524" layer="91"/>
<wire x1="231.14" y1="106.68" x2="231.14" y2="104.14" width="0.1524" layer="91"/>
<junction x="231.14" y="106.68"/>
<pinref part="U2" gate="G$1" pin="DIE_PAD"/>
<wire x1="231.14" y1="104.14" x2="228.6" y2="104.14" width="0.1524" layer="91"/>
<wire x1="231.14" y1="104.14" x2="231.14" y2="99.06" width="0.1524" layer="91"/>
<junction x="231.14" y="104.14"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="259.08" y1="182.88" x2="276.86" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="185.42" y1="12.7" x2="185.42" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="469.9" y1="78.74" x2="459.74" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="251.46" y1="172.72" x2="251.46" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="GND"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="38.1" y1="187.96" x2="53.34" y2="187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="A" pin="1"/>
<wire x1="157.48" y1="81.28" x2="142.24" y2="81.28" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V310" gate="G$1" pin="+3V3"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="144.78" y1="190.5" x2="157.48" y2="190.5" width="0.1524" layer="91"/>
<wire x1="157.48" y1="190.5" x2="182.88" y2="190.5" width="0.1524" layer="91"/>
<wire x1="182.88" y1="190.5" x2="187.96" y2="190.5" width="0.1524" layer="91"/>
<junction x="182.88" y="190.5"/>
<pinref part="U2" gate="G$1" pin="VCC_IO"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="157.48" y1="177.8" x2="157.48" y2="190.5" width="0.1524" layer="91"/>
<junction x="157.48" y="190.5"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="TERM1"/>
<wire x1="187.96" y1="35.56" x2="175.26" y2="35.56" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="ANLG-0/SER0-0(PA04)"/>
<wire x1="53.34" y1="154.94" x2="33.02" y2="154.94" width="0.1524" layer="91"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="+3V3"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<wire x1="38.1" y1="170.18" x2="53.34" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="A" pin="2"/>
<wire x1="157.48" y1="78.74" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="V+" class="0">
<segment>
<pinref part="P+4" gate="1" pin="V+"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="154.94" y1="213.36" x2="167.64" y2="213.36" width="0.1524" layer="91"/>
<wire x1="167.64" y1="213.36" x2="180.34" y2="213.36" width="0.1524" layer="91"/>
<junction x="167.64" y="213.36"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="180.34" y1="213.36" x2="193.04" y2="213.36" width="0.1524" layer="91"/>
<junction x="180.34" y="213.36"/>
<pinref part="C4" gate="G$1" pin="2"/>
<junction x="193.04" y="213.36"/>
<wire x1="193.04" y1="213.36" x2="205.74" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="205.74" y1="213.36" x2="218.44" y2="213.36" width="0.1524" layer="91"/>
<junction x="205.74" y="213.36"/>
<wire x1="218.44" y1="213.36" x2="231.14" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="231.14" y1="213.36" x2="243.84" y2="213.36" width="0.1524" layer="91"/>
<junction x="231.14" y="213.36"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="243.84" y1="213.36" x2="256.54" y2="213.36" width="0.1524" layer="91"/>
<junction x="243.84" y="213.36"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="256.54" y1="213.36" x2="269.24" y2="213.36" width="0.1524" layer="91"/>
<junction x="256.54" y="213.36"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="269.24" y1="213.36" x2="281.94" y2="213.36" width="0.1524" layer="91"/>
<junction x="269.24" y="213.36"/>
<pinref part="C5" gate="G$1" pin="2"/>
<junction x="218.44" y="213.36"/>
<pinref part="C7" gate="G$1" pin="2"/>
<junction x="281.94" y="213.36"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="281.94" y1="213.36" x2="294.64" y2="213.36" width="0.1524" layer="91"/>
<junction x="307.34" y="213.36"/>
<wire x1="294.64" y1="213.36" x2="307.34" y2="213.36" width="0.1524" layer="91"/>
<wire x1="307.34" y1="213.36" x2="320.04" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="320.04" y1="213.36" x2="332.74" y2="213.36" width="0.1524" layer="91"/>
<junction x="320.04" y="213.36"/>
<pinref part="C13" gate="G$1" pin="2"/>
<junction x="332.74" y="213.36"/>
<wire x1="332.74" y1="213.36" x2="345.44" y2="213.36" width="0.1524" layer="91"/>
<wire x1="345.44" y1="213.36" x2="358.14" y2="213.36" width="0.1524" layer="91"/>
<wire x1="358.14" y1="213.36" x2="365.76" y2="213.36" width="0.1524" layer="91"/>
<wire x1="365.76" y1="213.36" x2="388.62" y2="213.36" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="388.62" y1="213.36" x2="408.94" y2="213.36" width="0.1524" layer="91"/>
<wire x1="365.76" y1="210.82" x2="365.76" y2="213.36" width="0.1524" layer="91"/>
<junction x="365.76" y="213.36"/>
<pinref part="U$5" gate="G$1" pin="2"/>
<junction x="388.62" y="213.36"/>
<pinref part="C9" gate="G$1" pin="2"/>
<junction x="294.64" y="213.36"/>
<pinref part="C20" gate="G$1" pin="2"/>
<junction x="345.44" y="213.36"/>
<pinref part="C21" gate="G$1" pin="2"/>
<junction x="358.14" y="213.36"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VS"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="228.6" y1="190.5" x2="246.38" y2="190.5" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="246.38" y1="190.5" x2="259.08" y2="190.5" width="0.1524" layer="91"/>
<wire x1="241.3" y1="185.42" x2="246.38" y2="185.42" width="0.1524" layer="91"/>
<wire x1="246.38" y1="185.42" x2="246.38" y2="190.5" width="0.1524" layer="91"/>
<junction x="246.38" y="190.5"/>
<wire x1="259.08" y1="190.5" x2="276.86" y2="190.5" width="0.1524" layer="91"/>
<junction x="259.08" y="190.5"/>
<pinref part="P+1" gate="1" pin="V+"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="SP"/>
<wire x1="281.94" y1="119.38" x2="281.94" y2="127" width="0.1524" layer="91"/>
<wire x1="281.94" y1="127" x2="327.66" y2="127" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="SP"/>
<wire x1="327.66" y1="127" x2="327.66" y2="119.38" width="0.1524" layer="91"/>
<wire x1="327.66" y1="127" x2="350.52" y2="127" width="0.1524" layer="91"/>
<junction x="327.66" y="127"/>
<pinref part="Q3" gate="G$1" pin="SP"/>
<wire x1="350.52" y1="127" x2="375.92" y2="127" width="0.1524" layer="91"/>
<wire x1="375.92" y1="127" x2="375.92" y2="119.38" width="0.1524" layer="91"/>
<wire x1="375.92" y1="127" x2="421.64" y2="127" width="0.1524" layer="91"/>
<junction x="375.92" y="127"/>
<pinref part="Q4" gate="G$1" pin="SP"/>
<wire x1="421.64" y1="127" x2="421.64" y2="119.38" width="0.1524" layer="91"/>
<wire x1="350.52" y1="127" x2="350.52" y2="134.62" width="0.1524" layer="91"/>
<junction x="350.52" y="127"/>
<pinref part="P+2" gate="1" pin="V+"/>
</segment>
<segment>
<wire x1="469.9" y1="76.2" x2="459.74" y2="76.2" width="0.1524" layer="91"/>
<pinref part="P+5" gate="1" pin="V+"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="SENSE"/>
<pinref part="P+6" gate="1" pin="V+"/>
<wire x1="213.36" y1="30.48" x2="215.9" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TMC_EN" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ENABLE"/>
<label x="175.26" y="167.64" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="187.96" y1="167.64" x2="157.48" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="PWMBHS(PB11)"/>
<wire x1="53.34" y1="132.08" x2="35.56" y2="132.08" width="0.1524" layer="91"/>
<label x="35.56" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="TMC_CSN" class="0">
<segment>
<wire x1="187.96" y1="162.56" x2="175.26" y2="162.56" width="0.1524" layer="91"/>
<label x="175.26" y="162.56" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="CSN"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SER4-3(PB15)"/>
<wire x1="53.34" y1="101.6" x2="35.56" y2="101.6" width="0.1524" layer="91"/>
<label x="35.56" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="TMC_SCK" class="0">
<segment>
<wire x1="187.96" y1="160.02" x2="175.26" y2="160.02" width="0.1524" layer="91"/>
<label x="175.26" y="160.02" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="SCK"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SER4-1(PB13)"/>
<wire x1="53.34" y1="106.68" x2="35.56" y2="106.68" width="0.1524" layer="91"/>
<label x="35.56" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="TMC_MOSI" class="0">
<segment>
<wire x1="187.96" y1="157.48" x2="175.26" y2="157.48" width="0.1524" layer="91"/>
<label x="175.26" y="157.48" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="SDI"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SER4-0(PB12)"/>
<wire x1="53.34" y1="109.22" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
<label x="35.56" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="TMC_SG" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SG_TST"/>
<wire x1="187.96" y1="149.86" x2="175.26" y2="149.86" width="0.1524" layer="91"/>
<label x="175.26" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="PWMALS(PA08)"/>
<wire x1="53.34" y1="139.7" x2="35.56" y2="139.7" width="0.1524" layer="91"/>
<label x="35.56" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="TMC_MISO" class="0">
<segment>
<wire x1="187.96" y1="154.94" x2="175.26" y2="154.94" width="0.1524" layer="91"/>
<label x="175.26" y="154.94" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="SDO"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SER4-2(PB14)"/>
<wire x1="53.34" y1="104.14" x2="35.56" y2="104.14" width="0.1524" layer="91"/>
<label x="35.56" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="OA2" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="DN/DP"/>
<wire x1="345.44" y1="99.06" x2="353.06" y2="99.06" width="0.1524" layer="91"/>
<label x="347.98" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="BMA2"/>
<wire x1="228.6" y1="162.56" x2="241.3" y2="162.56" width="0.1524" layer="91"/>
<label x="231.14" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="469.9" y1="88.9" x2="459.74" y2="88.9" width="0.1524" layer="91"/>
<label x="459.74" y="88.9" size="1.778" layer="95"/>
<pinref part="U$8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="OA1" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="DN/DP"/>
<wire x1="299.72" y1="99.06" x2="307.34" y2="99.06" width="0.1524" layer="91"/>
<label x="302.26" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="BMA1"/>
<wire x1="228.6" y1="165.1" x2="241.3" y2="165.1" width="0.1524" layer="91"/>
<label x="231.14" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="469.9" y1="96.52" x2="459.74" y2="96.52" width="0.1524" layer="91"/>
<label x="459.74" y="96.52" size="1.778" layer="95"/>
<pinref part="U$7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="OB2" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="DN/DP"/>
<wire x1="439.42" y1="99.06" x2="447.04" y2="99.06" width="0.1524" layer="91"/>
<label x="441.96" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="BMB2"/>
<wire x1="228.6" y1="129.54" x2="241.3" y2="129.54" width="0.1524" layer="91"/>
<label x="231.14" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="469.9" y1="111.76" x2="459.74" y2="111.76" width="0.1524" layer="91"/>
<label x="459.74" y="111.76" size="1.778" layer="95"/>
<pinref part="U$4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="OB1" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="DN/DP"/>
<wire x1="393.7" y1="99.06" x2="401.32" y2="99.06" width="0.1524" layer="91"/>
<label x="396.24" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="BMB1"/>
<wire x1="228.6" y1="132.08" x2="241.3" y2="132.08" width="0.1524" layer="91"/>
<label x="231.14" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="469.9" y1="104.14" x2="459.74" y2="104.14" width="0.1524" layer="91"/>
<label x="459.74" y="104.14" size="1.778" layer="95"/>
<pinref part="U$6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VHS" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VHS"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="228.6" y1="185.42" x2="233.68" y2="185.42" width="0.1524" layer="91"/>
<label x="228.6" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="SRA" class="0">
<segment>
<wire x1="281.94" y1="73.66" x2="304.8" y2="73.66" width="0.1524" layer="91"/>
<label x="302.26" y="73.66" size="1.778" layer="95"/>
<pinref part="Q2" gate="G$1" pin="SN"/>
<wire x1="304.8" y1="73.66" x2="327.66" y2="73.66" width="0.1524" layer="91"/>
<wire x1="281.94" y1="73.66" x2="281.94" y2="76.2" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="SN"/>
<wire x1="327.66" y1="73.66" x2="327.66" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="304.8" y1="68.58" x2="304.8" y2="73.66" width="0.1524" layer="91"/>
<junction x="304.8" y="73.66"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SRA"/>
<wire x1="228.6" y1="149.86" x2="241.3" y2="149.86" width="0.1524" layer="91"/>
<label x="231.14" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="SRB" class="0">
<segment>
<wire x1="375.92" y1="73.66" x2="396.24" y2="73.66" width="0.1524" layer="91"/>
<label x="393.7" y="73.66" size="1.778" layer="95"/>
<pinref part="Q3" gate="G$1" pin="SN"/>
<wire x1="396.24" y1="73.66" x2="421.64" y2="73.66" width="0.1524" layer="91"/>
<wire x1="375.92" y1="73.66" x2="375.92" y2="76.2" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="SN"/>
<wire x1="421.64" y1="73.66" x2="421.64" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="396.24" y1="68.58" x2="396.24" y2="73.66" width="0.1524" layer="91"/>
<junction x="396.24" y="73.66"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SRB"/>
<wire x1="228.6" y1="116.84" x2="241.3" y2="116.84" width="0.1524" layer="91"/>
<label x="231.14" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="LB2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="LB2"/>
<wire x1="228.6" y1="124.46" x2="241.3" y2="124.46" width="0.1524" layer="91"/>
<label x="231.14" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q4" gate="G$1" pin="GN"/>
<wire x1="403.86" y1="88.9" x2="396.24" y2="88.9" width="0.1524" layer="91"/>
<label x="396.24" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="LB1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="LB1"/>
<wire x1="228.6" y1="121.92" x2="241.3" y2="121.92" width="0.1524" layer="91"/>
<label x="231.14" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q3" gate="G$1" pin="GN"/>
<wire x1="358.14" y1="88.9" x2="350.52" y2="88.9" width="0.1524" layer="91"/>
<label x="350.52" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="HB2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="HB2"/>
<wire x1="228.6" y1="137.16" x2="241.3" y2="137.16" width="0.1524" layer="91"/>
<label x="231.14" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q4" gate="G$1" pin="GP"/>
<wire x1="403.86" y1="106.68" x2="396.24" y2="106.68" width="0.1524" layer="91"/>
<label x="396.24" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="HB1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="HB1"/>
<wire x1="228.6" y1="139.7" x2="241.3" y2="139.7" width="0.1524" layer="91"/>
<label x="231.14" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q3" gate="G$1" pin="GP"/>
<wire x1="358.14" y1="106.68" x2="350.52" y2="106.68" width="0.1524" layer="91"/>
<label x="350.52" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="LA2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="LA2"/>
<wire x1="228.6" y1="157.48" x2="241.3" y2="157.48" width="0.1524" layer="91"/>
<label x="231.14" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="GN"/>
<wire x1="309.88" y1="88.9" x2="302.26" y2="88.9" width="0.1524" layer="91"/>
<label x="302.26" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="LA1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="LA1"/>
<wire x1="228.6" y1="154.94" x2="241.3" y2="154.94" width="0.1524" layer="91"/>
<label x="231.14" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="GN"/>
<wire x1="264.16" y1="88.9" x2="256.54" y2="88.9" width="0.1524" layer="91"/>
<label x="256.54" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="HA2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="HA2"/>
<wire x1="228.6" y1="170.18" x2="241.3" y2="170.18" width="0.1524" layer="91"/>
<label x="231.14" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="GP"/>
<wire x1="309.88" y1="106.68" x2="302.26" y2="106.68" width="0.1524" layer="91"/>
<label x="302.26" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="HA1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="HA1"/>
<wire x1="228.6" y1="172.72" x2="241.3" y2="172.72" width="0.1524" layer="91"/>
<label x="231.14" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="GP"/>
<wire x1="264.16" y1="106.68" x2="256.54" y2="106.68" width="0.1524" layer="91"/>
<label x="256.54" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="TMC_DIR" class="0">
<segment>
<wire x1="187.96" y1="177.8" x2="175.26" y2="177.8" width="0.1524" layer="91"/>
<label x="175.26" y="177.8" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="DIR"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="PWMAHS(PB10)"/>
<wire x1="53.34" y1="137.16" x2="35.56" y2="137.16" width="0.1524" layer="91"/>
<label x="35.56" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="TMC_STEP" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="STEP"/>
<wire x1="187.96" y1="180.34" x2="175.26" y2="180.34" width="0.1524" layer="91"/>
<label x="175.26" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="PWMBLS(PA09)"/>
<wire x1="53.34" y1="134.62" x2="35.56" y2="134.62" width="0.1524" layer="91"/>
<label x="35.56" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="RTD" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="TERM2"/>
<wire x1="187.96" y1="25.4" x2="185.42" y2="25.4" width="0.1524" layer="91"/>
<label x="175.26" y="25.4" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="185.42" y1="25.4" x2="175.26" y2="25.4" width="0.1524" layer="91"/>
<wire x1="185.42" y1="22.86" x2="185.42" y2="25.4" width="0.1524" layer="91"/>
<junction x="185.42" y="25.4"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="ANLG-1/SER0-1/DAC(PA05)"/>
<wire x1="53.34" y1="152.4" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
<label x="35.56" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="5VTMC" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="5VOUT"/>
<wire x1="228.6" y1="180.34" x2="251.46" y2="180.34" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<label x="228.6" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_MOSI" class="0">
<segment>
<pinref part="J1" gate="A" pin="6"/>
<wire x1="157.48" y1="68.58" x2="142.24" y2="68.58" width="0.1524" layer="91"/>
<label x="142.24" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SER1-0-SDA/TXD/MOSI-(PA16)"/>
<wire x1="53.34" y1="124.46" x2="35.56" y2="124.46" width="0.1524" layer="91"/>
<label x="35.56" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_MISO" class="0">
<segment>
<pinref part="J1" gate="A" pin="3"/>
<wire x1="157.48" y1="76.2" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<label x="142.24" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SER1-2-RXD/MISO-(PA18)"/>
<wire x1="53.34" y1="119.38" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<label x="35.56" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_CLK" class="0">
<segment>
<pinref part="J1" gate="A" pin="4"/>
<wire x1="157.48" y1="73.66" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<label x="142.24" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SER1-1-SCL/XCL/CLK-(PA17)"/>
<wire x1="53.34" y1="121.92" x2="35.56" y2="121.92" width="0.1524" layer="91"/>
<label x="35.56" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_CSN" class="0">
<segment>
<pinref part="J1" gate="A" pin="5"/>
<wire x1="157.48" y1="71.12" x2="142.24" y2="71.12" width="0.1524" layer="91"/>
<label x="142.24" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SER1-3-CSN-(PA19)"/>
<wire x1="53.34" y1="116.84" x2="35.56" y2="116.84" width="0.1524" layer="91"/>
<label x="35.56" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
