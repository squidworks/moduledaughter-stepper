# Squidworks Moduleboard Daughter: Stepper

This is a stepper driver for the [squidworks](https://gitlab.cba.mit.edu/squidworks/squidworks) project, that lives with one of the [moduleboards](https://gitlab.cba.mit.edu/squidworks/moduleboard-atsamd51) for the same. It uses a TMC262 bipolar chopper driver with 4 PN Pair FETs rated for ~ 8A, and features an SPI encoder breakout. Schematic and Board files are available in this repo. A revision is in the works for more intelligent connector placement.

![fab](moduledaughter-stepper-17/2019-11-18_stepper.jpg)
![routed](moduledaughter-stepper-17/routed.png)
![schem](moduledaughter-stepper-17/schematic.png)
